/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class reponsible for testing the schedulabe and batchable classes 
*   that update some fields on lead records
*
*   Test: DiasSemAtividadesBatch and DiasSemAtividadesScheduler
*   Call first: DiasSemAtividadesScheduler
*
* NAME: DiasSemAtividadesBatch_test
* AUTHOR: João Vitor Ramos                                      DATE: 21/10/2020
*******************************************************************************/
@isTest
public class DiasSemAtividadesBatch_test{
    @testSetup
    private static void setup(){
        //declaring lists to hadle the logic
        List<Lead> leadList = new List<Lead>();
        List<Task> taskList = new List<Task>();
        List<Event> eventList = new List<Event>();
        List<ContentNote> NoteList = new List<ContentNote>();
        List<ContentDocumentLink> documentLinkList = new List<ContentDocumentLink>();
        Integer counter=0;

        //creating Leads and its tasks, events and documents
        for(Integer i=0;i<10;i++){
            Lead ld = new Lead();
            ld.LastName = 'Lead'+i;
            ld.FirstName = 'Test';
            ld.Email = 'testlead'+i+'@test.com';
            ld.Status = 'Novo';
            ld.LeadSource = 'Anúncios';
            ld.Rating = 'Morno';
            ld.Company = 'TestCompany'+i;
            leadList.add(ld);
        }
        Insert leadList;

        for(Lead ld : leadList){
            Task tk = new Task();
            tk.WhoId = ld.Id;
            tk.Subject = ld.name+'Task';
            tk.OwnerId = '0050P000008bXceQAE';//Projeto werise user
            tk.Priority = 'Normal';
            tk.Status = 'Aberto';
            tk.ActivityDate = Date.today().addDays(-2+counter);
            counter++;
            taskList.add(tk);

            Event ev = new Event();
            ev.WhoId = ld.Id;
            ev.Subject = ld.name+'Event';
            ev.OwnerId = '0050P000008bXceQAE';//Projeto werise user
            ev.ActivityDate = Date.today().addDays(-2+counter);
            ev.DurationInMinutes = 60;
            ev.StartDateTime = Datetime.now().addDays(-2+counter);
            ev.EndDateTime = Datetime.now().addDays(-2+counter).addHours(1);
            eventList.add(ev);

            ContentNote cnt = new ContentNote();
            cnt.Content = Blob.valueof('Testing the note creation');
            cnt.Title = 'LeadNote';
            insert cnt;

            ContentDocumentLink cd = new ContentDocumentLink();
            cd.LinkedEntityId = ld.Id;
            cd.ContentDocumentId = cnt.id;
            insert cd;
        }
        insert taskList;
        insert eventList;
    }

    @isTest
    private static void testSchedulingBatch(){
            //creating crono variable to schedule the class
            String CRON_EXP = '0 0 0 15 3 ? 2022';
            
            Test.startTest();
            System.schedule('ScheduledApexTest', CRON_EXP, new DiasSemAtividadesScheduler());
            Test.stopTest();
    }
}
