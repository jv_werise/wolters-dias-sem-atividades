/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class reponsible for updtade some fields on lead records
*
*   Test class : DiasSemAtividadesBatch_test
*   Called from : DiasSemAtividadesScheduler
*
* NAME: DiasSemAtividadesBatch
* AUTHOR: João Vitor Ramos                                      DATE: 21/10/2020
*******************************************************************************/
global class DiasSemAtividadesBatch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        return Database.getQueryLocator(
            'SELECT Id, Name, Anexos__c, Compromissos__c, Tarefas__c, Dias_sem_atividades__c, ' +
            'ultimo_compromisso__c, Ultima_tarefa__c, Ultima_Tarefa_ou_Compromisso__c, CreatedDate ' +
            'FROM Lead'
        );
    }

    global void execute(Database.BatchableContext bc, List<Lead> records){
        // process each batch of 200 of records
        //creating list aggregate value lists of tasks, events and documents
        List<Id> leadId = new List<Id>();
        for(lead ld : records){
            leadId.add(ld.id);
        }

        List<Lead> leadUpdList = new List<Lead>();
        List<AggregateResult> taskList = [SELECT WhoId, COUNT(Id) countTask, MAX(Data_Atividade__c) ultimaTask FROM Task GROUP BY WhoId HAVING WhoId IN :leadId];
        List<AggregateResult> eventList = [SELECT WhoId, COUNT(Id) countEvent, MAX(Data_Atividade__c) ultimoEvent FROM Event GROUP BY WhoId HAVING WhoId IN :leadId];
        List<AggregateResult> attachmentNoteList = [SELECT LinkedEntityId, Count(ContentDocumentId) Anexos 
                                                        FROM ContentDocumentLink GROUP BY LinkedEntityId HAVING LinkedEntityId IN :leadId];
        
        //passing through the lead records, macthing the Lead Ids with the Activity and Document Ids
        //to update the Lead field values with the new arrangement values
        for(Lead ld : Records){
            if(!taskList.isEmpty()){
                for(AggregateResult tk : taskList){
                    if(ld.id == (Id)tk.get('WhoId')){
                        ld.Tarefas__c = (Integer)tk.get('countTask');
                        ld.Ultima_tarefa__c = (Date)tk.get('ultimaTask');
                        break;
                    }
                }
            }
            if(!eventList.isEmpty()){
                for(AggregateResult ev : eventList){
                    if(ld.id == (Id)ev.get('WhoId')){
                        ld.Compromissos__c = (Integer)ev.get('countEvent');
                        ld.Ultimo_Compromisso__c = (Date)ev.get('ultimoEvent');
                        break;
                    }
                }
            }
            if(!attachmentNoteList.isEmpty()){
                for(AggregateResult cdl : attachmentNoteList){
                    if(ld.id == (Id)cdl.get('LinkedEntityId')){
                        ld.Anexos__c = (Integer)cdl.get('Anexos');
                        break;
                    }
                }
            }

            //Parsing DateTime to Date value
            Date dataCriacao = date.newinstance(ld.CreatedDate.year(), ld.CreatedDate.month(), ld.CreatedDate.day());
            //populating the Ultima_tarefa_ou_compromisso__c field with the higher date
            if(ld.Ultima_tarefa__c > ld.Ultimo_Compromisso__c || (ld.Ultimo_Compromisso__c == null && ld.Ultima_tarefa__c != null)){
                ld.Ultima_tarefa_ou_compromisso__c = ld.Ultima_tarefa__c;
            }
            else if(ld.Ultimo_Compromisso__c > ld.Ultima_tarefa__c || (ld.Ultimo_Compromisso__c != null && ld.Ultima_tarefa__c == null)){
                ld.Ultima_tarefa_ou_compromisso__c = ld.Ultimo_Compromisso__c;
            }

            //populating the Dias_sem_atividades__c with the days between today and the higher date value
            if(ld.Ultima_tarefa_ou_compromisso__c >= Date.today() && ld.Ultima_tarefa_ou_compromisso__c != null){
                ld.Dias_sem_atividades__c = 0;
            }
            else if(ld.Ultima_tarefa_ou_compromisso__c < Date.today() && ld.Ultima_tarefa_ou_compromisso__c != null){          
                ld.Dias_sem_atividades__c = ld.Ultima_tarefa_ou_compromisso__c.daysBetween(Date.today());
            }
            else if(ld.Ultima_tarefa_ou_compromisso__c == null && dataCriacao < Date.today()){
                ld.Dias_sem_atividades__c = dataCriacao.daysBetween(Date.today());
            }
            leadUpdList.add(ld);
        }
        
        //updating the lead fields
        Database.SaveResult[] results = Database.update(leadUpdList, false);

        for(Database.SaveResult sr : results){
            if(sr.isSuccess()){
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully updated Lead. Lead ID: ' + sr.getId());
            } 
            else{
                // Operation failed, so get all errors
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Lead fields that affected this error: ' + err.getFields());
                }
            }
        }
    }

    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
        //Nothing to do after the execution for awhile
    }    
}
