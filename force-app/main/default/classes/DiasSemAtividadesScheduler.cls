/*******************************************************************************
*                               Werise - 2020
*-------------------------------------------------------------------------------
*
*	Class reponsible for call the batch class to updtade some fields on lead records
*
*   Test class : DiasSemAtividadesBatch_test
*   Call: DiasSemAtividadesBatch
*
* NAME: DiasSemAtividadesScheduler
* AUTHOR: João Vitor Ramos                                      DATE: 21/10/2020
*******************************************************************************/
global class DiasSemAtividadesScheduler implements Schedulable {
    global void execute(SchedulableContext ctx) {
        DiasSemAtividadesBatch dsa = new DiasSemAtividadesBatch();
        Id batchId = Database.executeBatch(dsa);
        System.debug(batchId);
    }
}


